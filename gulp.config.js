module.exports = function() {
    var config = {
        temp: './dist/',
        // all JS to vet
        alljs: [
            './app/lib/**/*.js',
            './app/js/**/*.js',
            './*.js'
        ],

        allcss: [
            './app/css/**/*.css',
            './**.css'
        ],

        injectCSS: [
            './dist/css/*.css'
        ],

        injectJS: [
            './dist/js/*.js'
        ],

        index: './app/index.html',

        serverWatch: './app/**/*.*'
    };


    return config;

};