'use strict';

describe('EditProfileControllerSpec', function() {

    var $controllerConstuctor, scope, mockGravatarUrlBuilder;
    beforeEach(module('eventsApp'));

    beforeEach(inject(function($controller, $rootScope) {
        $controllerConstuctor = $controller;
        scope = $rootScope.$new();
        mockGravatarUrlBuilder = sinon.stub({ buildGravatarUrl: function() {} })
    }));

    it('set the scope events to the result of eventData.getAllEvents', function() {

        $controllerConstuctor('EditProfileController', {
            '$scope': scope,
            gravatarUrlBuilder: mockGravatarUrlBuilder
        });

        var email = 'etancole@gmail.com';

        scope.getGravatarUrl(email);

        expect(mockGravatarUrlBuilder.buildGravatarUrl.calledWith(email)).toBe(true);
    });
});