'use strict';

describe('EventListController', function() {

    var $controllerConstuctor, scope, mockEventData;
    beforeEach(module('eventsApp'));

    beforeEach(inject(function($controller, $rootScope) {
        $controllerConstuctor = $controller;
        scope = $rootScope.$new();
        mockEventData = sinon.stub({ getAllEvents: function() {} })
    }));

    it('set the scope events to the result of eventData.getAllEvents', function() {

        var mockEvents = {};

        mockEventData.getAllEvents.returns(mockEvents);
        console.log('Ecents', mockEvents, 'EventData', mockEventData);
        $controllerConstuctor('EventListController', {
            '$scope': scope,
            eventData: mockEventData
        });

        expect(scope.events).toBe(mockEvents);
    });
});