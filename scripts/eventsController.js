var fs = require('fs');

module.exports.get = function(req, res) {
    var event = fs.readFileSync('app/data/event/' + req.params.id + '.json', 'utf8');
    res.setHeader('Content-type', 'application/json');
    res.send(event);
}

module.exports.save = function(req, res) {
    var event = req.data;
    fs.writeFileSync('app/data/event/' + req.params.id + '.json', Json.stringify(event));
    res.send(event);
}