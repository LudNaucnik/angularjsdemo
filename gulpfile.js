var gulp = require('gulp');
var args = require('yargs').argv;
var del = require('del');
var browserSync = require('browser-sync');
var wiredep = require('wiredep').stream;
var $ = require('gulp-load-plugins')({ lazy: true });
var config = require('./gulp.config.js')();

gulp.task('vet', function() {
    log('Analyzing source with JSHint and JSCS');
    log(config.alljs);

    return gulp
        .src(config.alljs)
        .pipe($.if(args.verbose, $.print()))
        .pipe($.jscs())
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish', { verbose: true }))
        .pipe($.jshint.reporter('fail'));
});

gulp.task('css-watcher', function() {
    startBrowserSync();
    gulp.watch([config.allcss], ['styles']);
});

gulp.task('serve', ['styles'], function() {
    log('Adding CSS and JS to index.html');
    return gulp
        .src(config.index)
        .pipe($.plumber())
        .pipe($.if(args.verbose, $.print()))
        .pipe($.inject(gulp.src(config.injectJS)))
        .pipe($.inject(gulp.src(config.injectCSS)))
        .pipe(gulp.dest(config.temp));
});

gulp.task('styles', ['clean-styles', 'js'], function() {
    log('Compiling CSS styles');
    return gulp
        .src(config.allcss)
        .pipe($.plumber())
        .pipe($.if(args.verbose, $.print()))
        .pipe($.autoprefixer({ browsers: ['last 2 version', '> 5%'] }))
        .pipe($.concat('allcss.css'))
        .pipe(gulp.dest(config.temp + './dist/css/'));
});

gulp.task('js', ['templates'], function() {
    log('Generating JS files');
    return gulp
        .src(config.alljs)
        .pipe($.concat('alljs.js'))
        .pipe(gulp.dest(config.temp + './dist/js/'));
});

gulp.task('templates', function() {
    log('Generating templates JS files');
    return gulp
        .src('./app/templates/**/*.html')
        .pipe(gulp.dest(config.temp + '/app/templates/'));
})

gulp.task('clean-styles', function() {
    var files = config.temp + '*.css';
    clean(files);
});

////////////

function startBrowserSync() {
    if (browserSync.active) {
        return;
    }

    log('Starting browser-sync on port ' + '9000');

    var options = {
        proxy: 'localhost:' + '9000',
        port: 3500,
        files: ['**/*.*'],
        ghostMode: {
            clicks: true,
            location: true,
            forms: true,
            scroll: true
        },
        injectChanges: true,
        logFileChanges: true,
        logLevel: 'debug',
        logPrefix: 'gulp-patterns',
        notify: true,
        reloadDelay: 1000
    };

    browserSync(options);
}

function clean(path) {
    log('Cleaning: ' + $.util.colors.blue(path));
    del(path);
}

function log(msg) {
    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.blue(msg[item]));
            }
        }
    } else {
        $.util.log($.util.colors.blue(msg));
    }
}