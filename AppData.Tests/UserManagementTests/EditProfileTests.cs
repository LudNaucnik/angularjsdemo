﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppData.Models;
using AppData.DAL;

namespace AppData.Tests.UserManagementTests
{
    [TestClass]
    public class EditProfileTests
    {
        [TestMethod]
        public void goodUsernameEditProfile()
        {
            User usr = new User
            {
                Username = "admin",
                Name = "admin",
                Surname = "admin",
                BirthDate = Convert.ToDateTime("2017/01/02"),
                EMail = "admin@admin.com"
            };
            Boolean res = UserActions.EditUserProfile(usr);
            Assert.IsTrue(res);
        }

        [TestMethod]
        public void badUsernameEditProfile()
        {
            User usr = new User
            {
                Username = ""
            };
            Boolean res = UserActions.EditUserProfile(usr);
            Assert.IsFalse(res);
        }
    }
}
