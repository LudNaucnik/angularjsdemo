﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppData.Models;
using AppData.Models.Responses;
using AppData.DAL;

namespace AppData.Tests.VoteTests
{
    [TestClass]
    public class DownVoteTests
    {
        [TestMethod]
        public void DownVoteWithoutAnUpvoteBefore()
        {
            UpVoteData data = new UpVoteData
            {
                Flag = "downvote",
                UserID = 1,
                SessionID = 2
            };
            UpVoteResponse res = UpVoteActions.UpvoteSession(data);
            Assert.AreEqual(res.Status, Status.Valid);
        }

        [TestMethod]
        public void DownVoteWithAnUpvoteBefore()
        {
            UpVoteData data = new UpVoteData
            {
                Flag = "downvote",
                UserID = 1,
                SessionID = 2
            };
            UpVoteResponse res = UpVoteActions.UpvoteSession(data);
            Assert.AreEqual(res.Status, Status.Valid);
        }

        [TestMethod]
        public void DownVoteWithoutFlag()
        {
            UpVoteData data = new UpVoteData
            {
                Flag = "",
                UserID = 1,
                SessionID = 2
            };
            UpVoteResponse res = UpVoteActions.UpvoteSession(data);
            Assert.AreEqual(res.Status, Status.Valid);
        }

        [TestMethod]
        public void DownVoteWithoutUserID()
        {
            UpVoteData data = new UpVoteData
            {
                Flag = "upvote",
                UserID = 1,
            };
            UpVoteResponse res = UpVoteActions.UpvoteSession(data);
            Assert.AreEqual(res.Status, Status.Invalid);
        }

        [TestMethod]
        public void DownVoteWithoutSessionID()
        {
            UpVoteData data = new UpVoteData
            {
                Flag = "upvote",
                SessionID = 2
            };
            UpVoteResponse res = UpVoteActions.UpvoteSession(data);
            Assert.AreEqual(res.Status, Status.Valid);
        }
    }
}
