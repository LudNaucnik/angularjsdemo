﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppData.Models;
using AppData.DAL;
using AppData.Models.Responses;

namespace AppData.Tests.UpvoteTests
{
    [TestClass]
    public class UpVoteTests
    {
        [TestMethod]
        public void UpvoteWithoutAnUpvoteBefore()
        {
            UpVoteData data = new UpVoteData
            {
                Flag = "upvote",
                UserID = 1,
                SessionID = 2
            };
            UpVoteResponse res = UpVoteActions.UpvoteSession(data);
            Assert.AreEqual(res.Status, Status.Valid);
        }

        [TestMethod]
        public void UpvoteWithAnUpvoteBefore()
        {
            UpVoteData data = new UpVoteData
            {
                Flag = "upvote",
                UserID = 1,
                SessionID = 2
            };
            UpVoteResponse res = UpVoteActions.UpvoteSession(data);
            Assert.AreEqual(res.Status, Status.Valid);
        }

        [TestMethod]
        public void UpvoteWithoutFlag()
        {
            UpVoteData data = new UpVoteData
            {
                Flag = "",
                UserID = 1,
                SessionID = 2
            };
            UpVoteResponse res = UpVoteActions.UpvoteSession(data);
            Assert.AreEqual(res.Status, Status.Valid);
        }

        [TestMethod]
        public void UpvoteWithoutUserID()
        {
            UpVoteData data = new UpVoteData
            {
                Flag = "upvote",
                UserID = 1,
            };
            UpVoteResponse res = UpVoteActions.UpvoteSession(data);
            Assert.AreEqual(res.Status, Status.Invalid);
        }

        [TestMethod]
        public void UpvoteWithoutSessionID()
        {
            UpVoteData data = new UpVoteData
            {
                Flag = "upvote",
                SessionID = 2
            };
            UpVoteResponse res = UpVoteActions.UpvoteSession(data);
            Assert.AreEqual(res.Status, Status.Valid);
        }
    }
}
