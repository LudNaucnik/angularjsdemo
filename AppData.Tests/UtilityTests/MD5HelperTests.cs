﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppData.Utility;

namespace AppData.Tests.UtilityTests
{
    [TestClass]
    public class MD5HelperTests
    {
        [TestMethod]
        public void GoodMD5String()
        {
            String md5 = @"9e164d715bb4f9861b0865f21598646b";
            Boolean res = MD5Helper.IsValidMD5(md5);
            Assert.IsTrue(res);
        }

        [TestMethod]
        public void BadMD5String()
        {
            String md5 = @"9e164d715bb4weferhg[wjerpgqoefjq[owejf9uqwer9[[[[[f9awdawd861b0865f21598646b";
            Boolean res = MD5Helper.IsValidMD5(md5);
            Assert.IsFalse(res);
        }
    }
}
