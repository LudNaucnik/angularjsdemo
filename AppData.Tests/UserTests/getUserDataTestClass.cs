﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppData.DAL;
using AppData.Models;

namespace AppData.Tests.LoginTests
{
    [TestClass]
    public class getUserDataTestClass
    {
        [TestMethod]
        public void getDataWithCorrectUsername()
        {
            User usr = new User
            {
                Username = "admin",
                Name = "admin"
            };
            User res = UserActions.getUserData(usr);
            Assert.AreSame(usr.Name, res.Name);
        }

        [TestMethod]
        public void getDataWithWrongUsername()
        {
            User usr = new User
            {
                Username = "",
                Name = "admin"
            };
            User res = UserActions.getUserData(usr);
            Assert.AreNotSame(usr.Name, res.Name);
        }
    }
}
