﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppData.DAL;

namespace AppData.Tests.LoginTests
{
    [TestClass]
    public class UsernameCheckTestClass
    {
        [TestMethod]
        public void findUsernameByExistingUsername()
        {
            Boolean res = UserActions.CheckUsername("admin");
            Assert.IsTrue(res);
        }

        [TestMethod]
        public void findUsernameByNotExistingUsername()
        {
            Boolean res = UserActions.CheckUsername("");
            Assert.IsFalse(res);
        }
    }
}
