﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppData.Models;
using AppData.Controllers;
using AppData.Models.Responses;

namespace AppData.Tests.UserTests
{
    [TestClass]
    public class CheckingUserUsernameAndPasswordTests
    {
        [TestMethod]
        public void GetUserCorrectUsernameAndCorrectPassword()
        {
            LoginController ctrl = new LoginController();
            User usr = new User
            {
                Username = "admin",
                Password = "21232f297a57a5a743894a0e4a801fc3"
            };
            UserResponse res = ctrl.getUser(usr);
            Assert.AreEqual(res.Status, Status.Valid);
        }

        [TestMethod]
        public void GetUserCorrectUsernameAndWrongPassword()
        {
            LoginController ctrl = new LoginController();
            User usr = new User
            {
                Username = "admin",
                Password = ""
            };
            UserResponse res = ctrl.getUser(usr);
            Assert.AreEqual(res.Status, Status.Invalid);
        }

        [TestMethod]
        public void GetUserWithoutUsernameAndWrongPassword()
        {
            LoginController ctrl = new LoginController();
            User usr = new User
            {
                Username = "admin",
                Password = ""
            };
            UserResponse res = ctrl.getUser(usr);
            Assert.AreEqual(res.Status, Status.Invalid);
        }

        [TestMethod]
        public void GetUserWithoutUsernameAndCorrentPassword()
        {
            LoginController ctrl = new LoginController();
            User usr = new User
            {
                Username = "",
                Password = "21232f297a57a5a743894a0e4a801fc3"
            };
            UserResponse res = ctrl.getUser(usr);
            Assert.AreEqual(res.Status, Status.Invalid);
        }

        [TestMethod]
        public void GetUserWithoutUsernameAndWitoutPassword()
        {
            LoginController ctrl = new LoginController();
            User usr = new User
            {
                Username = "",
                Password = ""
            };
            UserResponse res = ctrl.getUser(usr);
            Assert.AreEqual(res.Status, Status.Invalid);
        }
    }
}
