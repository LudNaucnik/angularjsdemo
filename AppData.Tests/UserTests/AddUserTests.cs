﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppData.Controllers;
using AppData.Models;
using AppData.Models.Responses;

namespace AppData.Tests.UserTests
{
    [TestClass]
    public class AddUserTests
    {
        [TestMethod]
        public void AddGoodUserTest()
        {
            LoginController ctrl = new LoginController();
            User usr = new User
            {
                Username = "admin",
                Password = "21232f297a57a5a743894a0e4a801fc3",
                Name = "admin",
                Surname = "admin",
                BirthDate = DateTime.Now,
                EMail = "admin@admin.com"
            };
            UserResponse res = ctrl.register(usr);
            if (!res.Message.Contains("duplicate username:") && !res.Message.Contains("user created:"))
            {
                Assert.Fail();
            }

        }

        [TestMethod]
        public void AddBaduserTest()
        {
            LoginController ctrl = new LoginController();
            User usr = new User
            {
                Username = "admin",
                Password = "awdawdawd",
                Name = "admin",
                Surname = "admin",
                BirthDate = DateTime.Now,
                EMail = "admin@admin.com"
            };
            UserResponse res = ctrl.register(usr);
            Assert.AreEqual(res.Status, Status.Invalid);
        }
    }
}
