﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppData.Models;
using AppData.DAL;

namespace AppData.Tests.UserTests
{
    [TestClass]
    public class ChangePasswordTests
    {
        [TestMethod]
        public void ChangePasswordGoodUsernameAndPassword()
        {
            ChangePasswordData data = new ChangePasswordData
            {
                OldPassword = "21232f297a57a5a743894a0e4a801fc3",
                NewPassword = "21232f297a57a5a743894a0e4a801fc3",
                Username = "admin"
            };
            Boolean res = UserActions.ChangePassword(data);
            Assert.IsTrue(res);
        }

        [TestMethod]
        public void ChangePasswordGoodUsernameAndBadPassword()
        {
            ChangePasswordData data = new ChangePasswordData
            {
                OldPassword = "21232f297a57a5a743894a0e4a801fc3",
                NewPassword = "21232f297a57a5a743894a0e4a801fc3",
                Username = "admin"
            };
            Boolean res = UserActions.ChangePassword(data);
            Assert.IsTrue(res);
        }

        [TestMethod]
        public void ChangePasswordGoodUsernameAndEmptyPassword()
        {
            ChangePasswordData data = new ChangePasswordData
            {
                OldPassword = "21232f297a57a5a743894a0e4a801fc3",
                NewPassword = "",
                Username = "admin"
            };
            Boolean res = UserActions.ChangePassword(data);
            Assert.IsFalse(res);
        }

        [TestMethod]
        public void ChangePasswordBadUsernameAndEmptyPassword()
        {
            ChangePasswordData data = new ChangePasswordData
            {
                OldPassword = "21232f297a57a5a743894a0e4a801fc3",
                NewPassword = "",
                Username = ""
            };
            Boolean res = UserActions.ChangePassword(data);
            Assert.IsFalse(res);
        }

        [TestMethod]
        public void ChangePasswordBadUsernameAndGoodPassword()
        {
            ChangePasswordData data = new ChangePasswordData
            {
                OldPassword = "21232f297a57a5a743894a0e4a801fc3",
                NewPassword = "21232f297a57a5a743894a0e4a801fc3",
                Username = ""
            };
            Boolean res = UserActions.ChangePassword(data);
            Assert.IsFalse(res);
        }

        [TestMethod]
        public void ChangePasswordGoodUsernameAndEmptyOldPassword()
        {
            ChangePasswordData data = new ChangePasswordData
            {
                OldPassword = "",
                NewPassword = "21232f297a57a5a743894a0e4a801fc3",
                Username = "admin"
            };
            Boolean res = UserActions.ChangePassword(data);
            Assert.IsFalse(res);
        }

        [TestMethod]
        public void ChangePasswordOnlyNewPassword()
        {
            ChangePasswordData data = new ChangePasswordData
            {
                OldPassword = "",
                NewPassword = "21232f297a57a5a743894a0e4a801fc3",
                Username = "admin"
            };
            Boolean res = UserActions.ChangePassword(data);
            Assert.IsFalse(res);
        }
    }
}
