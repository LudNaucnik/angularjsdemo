'use strict';

eventsApp.factory('upVoteService', function($resource) {
    var endPoint = 'http://localhost:10000';
    return {
        vote: function(session) {
            var postData = {
                UserID: session.userID,
                Flag: session.flag,
                SessionID: session.sessionID
            };
            return ($resource(endPoint + '/VoteSession', {
                'save': {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            }).save(postData)).$promise;
        }
    };

});