'use strict';

eventsApp.factory('userService', function($resource, MD5Service, $rootScope) {
    var endPoint = 'http://localhost:10000/';

    return {

        getUser: function(user) {
            var postData = {
                Username: user.username,
                Password: MD5Service.MD5(user.password)
            };
            return ($resource(endPoint + '/getUser', {
                'save': {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            }).save(postData)).$promise;
        },

        regUser: function(user) {
            var postData = {
                Username: user.Username,
                Password: MD5Service.MD5(user.Password),
                Name: user.Name,
                Surname: user.Surname,
                EMail: user.EMail,
                BirthDate: user.BirthDate
            };
            return ($resource(endPoint + '/register', {
                'save': {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            }).save(postData)).$promise;
        },

        changePasswordUser: function(user) {
            var postData = {
                Username: user.username,
                OldPassword: MD5Service.MD5(user.oldPassword),
                NewPassword: MD5Service.MD5(user.newPassword)
            };
            return ($resource(endPoint + '/changepassword', {
                'save': {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            }).save(postData)).$promise;
        },

        editProfileUser: function(profile) {
            var postData = {
                Username: profile.username,
                Password: MD5Service.MD5(profile.password),
                Name: profile.Name,
                Surname: profile.Surname,
                EMail: profile.EMail,
                BirthDate: profile.BirthDate
            };
            return ($resource(endPoint + '/edituserprofile', {
                'save': {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            }).save(postData)).$promise;
        },

        getuserData: function(username) {
            var postData = {
                Username: username
            };
            return ($resource(endPoint + '/getuserdata', {
                'save': {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            }).save(postData)).$promise;
        }
    };
});