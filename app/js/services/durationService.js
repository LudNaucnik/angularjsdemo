'use strict';

eventsApp.factory('durationService', function() {
    return {
        getDurations: function() {
            return [{ key: 1, value: '30 min' }, { key: 2, value: '1 hour' },
                { key: 3, value: '2 hours' }, { key: 4, value: '3 hours' }
            ];
        },

        getLevels: function() {
            return ['Advanced', 'Introductory', 'Intermediate'];
        }
    };
});