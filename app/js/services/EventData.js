'use strict';

eventsApp.factory('eventData', function($resource) {
    var endPoint = 'http://localhost:10000';
    return {

        getEvent: function(eventId) {
            // return $http({ method: 'GET', url: 'http://localhost:10000/event/1' });

            return $resource(endPoint + '/event/:id', { id: '@id' }).get({ id: eventId });
        },

        saveEvent: function(event) {
            return $resource(endPoint + '/saveEvent', {
                'save': {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            }).save(event);
        },

        editEvent: function(event) {
            return $resource(endPoint + '/editevent', {
                'save': {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            }).save(event);
        },

        getAllEvents: function() {
            return $resource(endPoint + '/getAllEvents').query().$promise;
        }
    };
});