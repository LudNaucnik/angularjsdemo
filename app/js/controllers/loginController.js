'use strict';

eventsApp.controller('logInController', function logInController($scope, $location, userService, $rootScope,
    $filter, $cookies) {
    $scope.user = {};
    $scope.rememberMeUser = false;
    $scope.submit = function() {
        $scope.user.username = $filter('lowercase')($scope.user.username);
        $scope.promise = userService.getUser($scope.user);
        $scope.promise = $scope.promise.then(function(user) {
                switch (user.Status) {
                    case 0:
                        {
                            $rootScope.isLoggedIn = '0';
                            $rootScope.loggedInUsername = $scope.user.username;
                            $cookies.put('loggedInUsername', $scope.user.username);
                            $location.path('/events');
                            break;
                        }
                    case 1:
                        {
                            $rootScope.isLoggedIn = '1';
                            $scope.errorMessage = 'Invalid Username and password';
                            console.log('Failed');
                            break;
                        }
                    default:
                        {
                            $rootScope.isLoggedIn = '1';
                            $scope.errorMessage = 'Error in login';
                            break;
                        }
                }
            })
            .catch(function(response) { console.log(response); });
    };

    $scope.enterPressed = function(keyEvent) {
        if (keyEvent.which === 13) {
            $scope.submit();
        }
    };
});