'use strict';

eventsApp.controller('changePasswordController', function($scope, userService, $location, $rootScope) {
    $scope.submit = function(user, changePassForm) {
        if (changePassForm.$valid) {
            if ($scope.user.newPassword !== $scope.confirmPassword) {
                $scope.errorMessage = 'Password do not match';
            } else {
                user.username = $rootScope.loggedInUsername;
                userService.changePasswordUser(user).then(function(user) {
                    if (user.Status !== 0) {
                        $scope.errorMessage = 'Error Changing the password';
                    } else {
                        $location.path('/events');
                    }
                }).catch(function(response) { console.log(response); });

            }
        }
    };
});