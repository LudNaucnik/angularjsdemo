'use strict';

eventsApp.controller('EventController', function EventController($scope, eventData, $routeParams,
    durationService, $filter, upVoteService, $rootScope, userService) {

    $scope.sortOrder = 'name';
    $scope.levels = durationService.getLevels();
    var userID = 0;
    userService.getuserData($rootScope.loggedInUsername)
        .then(function(response) {
            userID = response.id;
        }).catch(function(response) { console.log(response); });

    $scope.event = eventData.getEvent($routeParams.eventId).$promise;
    $scope.event = $scope.event.then(function(event) {
            event.date = $filter('date')(event.date, 'MM/dd/yyyy');
            $scope.event = event;
        })
        .catch(function(response) { console.log(response); });

    $scope.upVoteSession = function(session) {
        var data = {
            userID: userID,
            flag: 'upvote',
            sessionID: session.id
        };
        upVoteService.vote(data)
            .then(function(response) {
                if (response.Status === 0) {
                    if (response.VoteStatus === 2) {
                        session.upVoteCount += 2;
                    } else if (response.VoteStatus === 3) {
                        session.upVoteCount--;
                    } else {
                        session.upVoteCount++;
                    }
                } else {
                    $scope.errorMessageVote = 'Already Upvoted this session';
                }
            }).catch(function(response) { console.log(response); });
    };

    $scope.downVoteSession = function(session) {
        var data = {
            userID: userID,
            flag: 'downvote',
            sessionID: session.id
        };
        upVoteService.vote(data)
            .then(function(response) {
                if (response.Status === 0) {
                    if (response.VoteStatus === 2) {
                        session.upVoteCount -= 2;
                    } else if (response.VoteStatus === 3) {
                        session.upVoteCount++;
                    } else {
                        session.upVoteCount--;
                    }
                } else {
                    $scope.errorMessageVote = 'Already Down Voted this session';
                }
            }).catch(function(response) { console.log(response); });
    };


});