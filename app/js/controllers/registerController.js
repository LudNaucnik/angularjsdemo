'use strict';

eventsApp.controller('registerController', function registerController($scope, $location, userService, $rootScope,
    $filter, $cookies) {
    $scope.user = {};
    $scope.submit = function() {
        if (!this.RegForm.$valid) { return; }
        $scope.user.username = $filter('lowercase')($scope.user.username);
        return userService.regUser($scope.user)
            .then(function(user) {
                $scope.clearErrorMessages();
                if (user.Status === 0) {
                    $rootScope.loggedInUsername = $scope.user.username;
                    $cookies.put('loggedInUsername', $scope.user.username);
                    $rootScope.isLoggedIn = 0;
                    $location.path('/events');
                } else {
                    $rootScope.isLoggedIn = 1;
                    $scope.setErrorMessages(user.ValidationErrors);
                }
            })
            .catch(function(response) { console.log(response); });
    };

    $scope.enterPressed = function(keyEvent) {
        if (keyEvent.which === 13) {
            $scope.submit();
        }
    };

    $scope.clearErrorMessages = function() {
        $scope.RegForm.username.errorMessage = undefined;
        $scope.RegForm.email.errorMessage = undefined;
        $scope.RegForm.name.errorMessage = undefined;
        $scope.RegForm.date.errorMessage = undefined;
        $scope.RegForm.password.errorMessage = undefined;
        $scope.RegForm.surname.errorMessage = undefined;
    };

    $scope.setErrorMessages = function(data) {
        angular.forEach(data, function(value, key) {
            if (value.modelFieldKey === 'user.Username') {
                $scope.RegForm.username.errorMessage = value.Message;
            }
            if (value.modelFieldKey === 'user.EMail') {
                $scope.RegForm.email.errorMessage = value.Message;
            }
            if (value.modelFieldKey === 'user.BirthDate') {
                $scope.RegForm.date.errorMessage = value.Message;
            }
            if (value.modelFieldKey === 'user.Name') {
                $scope.RegForm.name.errorMessage = value.Message;
            }
            if (value.modelFieldKey === 'user.Surname') {
                $scope.RegForm.surname.errorMessage = value.Message;
            }
            if (value.modelFieldKey === 'user.Password') {
                $scope.RegForm.password.errorMessage = value.Message;
            }
        });
    }
});