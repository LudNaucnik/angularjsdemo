'use strict';

eventsApp.controller('EditEventController', function EditEventController($scope, eventData, durationService, $routeParams, $filter, $location) {

    $scope.event = {};
    $scope.session = {};
    $scope.event.sessions = [];
    $scope.durations = durationService.getDurations();
    $scope.levels = durationService.getLevels();

    if ($routeParams.eventId != undefined) {
        $scope.event = eventData.getEvent($routeParams.eventId).$promise;
        $scope.event = $scope.event.then(function(response) {
            response.date = $filter('date')(response.date, 'MM/dd/yyyy');
            $scope.event = response;
        }).catch(function(response) { console.log(response); })
    }

    $scope.saveEvent = function(event, newEventForm) {

        if (!newEventForm.$valid) {
            return;
        }
        if ($routeParams.eventId) {
            eventData.editEvent(event).$promise
                .then(function(response) { console.log('Success', response); })
                .catch(function(response) { console.log('Failure', response); });
            $location.path("event/" + $routeParams.eventId);
            return;
        }
        eventData.saveEvent(event).$promise
            .then(function(response) { console.log('Success', response); })
            .catch(function(response) { console.log('Failure', response); });
    };

    $scope.addSession = function(session, sessionForm) {
        if (sessionForm.$valid) {
            $scope.event.sessions.push(session);
            $scope.session = {};
        }
    };

    $scope.removeSession = function(index) {
        $scope.event.sessions.splice(index, 1);
    };

    $scope.cancelEdit = function() {
        window.location = '/events';
    };
});