'use strict';

eventsApp.controller('homeController', function homeController($scope, $rootScope, $location, $cookies) {
    $scope.logOut = function() {
        $rootScope.isLoggedIn = 1;
        $cookies.remove('login');
        $location.path('/login');
    };
    // $scope.$watch(function(isLoggedIn) { return $rootScope.isLoggedIn },
    //     function(newValue, oldValue) {
    //         $scope.isLoggedIn = newValue;
    //     }
    // );
    // $scope.isLoggedIn = $rootScope.isLoggedIn;
});