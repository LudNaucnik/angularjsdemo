'use strict';

eventsApp.controller('EditProfileController', function EditProfileController($scope, userService,
    $rootScope, $filter, $location) {
    userService.getuserData($rootScope.loggedInUsername)
        .then(function(response) {
            response.BirthDate = $filter('date')(response.BirthDate, 'MM/dd/yyyy');
            $scope.profile = response;
        }).catch(function(response) { console.log(response); });
    $scope.submit = function(profile, editProfileForm) {
        profile.username = $rootScope.loggedInUsername;
        userService.getUser(profile)
            .then(function(user) {
                switch (user.Status) {
                    case 0:
                        {
                            $rootScope.isLoggedIn = '0';
                            userService.editProfileUser(profile)
                            .then(function(response) {
                                if (response.Status === '0') {
                                    $location.path('/events');
                                } else {
                                    $scope.errorMessage = 'Error in editing profile';
                                }
                            }).catch(function(response) { console.log(response); });
                            break;
                        }
                    case 1:
                        {
                            $rootScope.isLoggedIn = '1';
                            $scope.errorMessage = 'Invalid Username and password';
                            break;
                        }
                    default:
                        {
                            $rootScope.isLoggedIn = '1';
                            $scope.errorMessage = 'Error in login';
                            break;
                        }
                }
            })
            .catch(function(response) { console.log(response); });
    };
});