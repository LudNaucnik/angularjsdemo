'use strict';

eventsApp.controller('EventListController', function EventListController($scope, eventData) {
    $scope.events = eventData.getAllEvents();
    $scope.events = $scope.events.then(function(response) {
        $scope.events = response;
    }).catch(function(response) { console.log(response); });
});