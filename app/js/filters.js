'use strict';

eventsApp.filter('durations', function(durationService) {
    return function(duration) {
        var dur = durationService.getDurations();
        for (var i = 0; i < dur.length; i++) {
            if (dur[i].key === duration) {
                return dur[i].value;
            }
        }
        // switch (duration) {
        //     case 1:
        //         return '30 minites';
        //     case 2:
        //         return '1 hour';
        //     case 3:
        //         return '2 hours';
        //     case 4:
        //         return '3 hours';
        //     default:
        //         break;
        // }
    };

});