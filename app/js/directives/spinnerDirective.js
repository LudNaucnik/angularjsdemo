'use strict';

eventsApp.directive('spinner', function() {
    return {
        restrict: 'E',
        templateUrl: '/templates/directives/spinner.html'
    };
});