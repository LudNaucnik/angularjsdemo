'use strict';

eventsApp.directive('collapsable', function() {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="well" ng-click="toggleVisibility()"><h4 class="well-title">{{title}}</h4><div ng-show="visible" ng-transclude></div></div>',
        transclude: true,
        controller: function($scope) {
            $scope.visible = false;
            $scope.toggleVisibility = function() {
                $scope.visible = !$scope.visible;
            };
        },
        scope: {
            title: '@'
        }
    };
});