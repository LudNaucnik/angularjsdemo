'use strict';

var eventsApp = angular.module('eventsApp', ['ngResource', 'ngRoute', 'ngCookies'])
    .config(function($routeProvider, $locationProvider) {
        $routeProvider.when('/newEvent', {
            templateUrl: '/templates/NewEvent.html',
            controller: 'EditEventController'
        });
        $routeProvider.when('/events', {
            templateUrl: '/templates/EventList.html',
            controller: 'EventListController'
        });
        $routeProvider.when('/event/:eventId', {
            templateUrl: '/templates/EventDetails.html',
            controller: 'EventController'
        });
        $routeProvider.when('/edit/:eventId', {
            templateUrl: '/templates/NewEvent.html',
            controller: 'EditEventController'
        });
        $routeProvider.when('/editProfile', {
            templateUrl: '/templates/EditProfile.html',
            controller: 'EditProfileController'
        });
        $routeProvider.when('/login', {
            templateUrl: '/templates/login.html',
            controller: 'logInController'
        });
        $routeProvider.when('/register', {
            templateUrl: '/templates/register.html',
            controller: 'registerController'
        });
        $routeProvider.when('/changePassword', {
            templateUrl: '/templates/changePassword.html',
            controller: 'changePasswordController'
        });
        $routeProvider.otherwise({ redirectTo: '/events' });
        $locationProvider.html5Mode({
            enabled: true
        });
    }).run(function($rootScope, $location, $cookies) {
        $rootScope.isLoggedIn = $cookies.get('login');
        $rootScope.loggedInUsername = $cookies.get('loggedInUsername');
        $rootScope.$on('$locationChangeStart', function(event, next, current) {
            if ($location.path() !== '/register') {
                if ($rootScope.isLoggedIn !== '0') {
                    $location.path('/login');
                } else {
                    $cookies.put('login', '0');
                }
            }
        });
    });

eventsApp.config(['$provide', function($provide) {
    function httpPromise(o, status) {
        var promise = (o && ((o.promise && o.promise()) || o));
        return promise && promise.$$state ? promise.$$state.status === status : 0;
    }
    $provide.decorator('$rootScope', ['$delegate', function($delegate) {
        var rootScopePrototype = Object.getPrototypeOf($delegate);
        rootScopePrototype.$resolved = function(o) {
            return httpPromise(o, 1);
        };
        rootScopePrototype.$rejected = function(o) {
            return httpPromise(o, 2);
        };
        rootScopePrototype.$pending = function(o) {
            return httpPromise(o, 0);
        };
        return $delegate;
    }]);
}]);