﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppData.Utility
{
    public static class MD5Helper
    {
        public static bool IsValidMD5(string md5)
        {
            if (md5 == null || md5.Length != 32) return false;
            foreach (var x in md5)
            {
                if ((x < '0' || x > '9') && (x < 'a' || x > 'f') && (x < 'A' || x > 'F'))
                {
                    return false;
                }
            }
            return true;
        }
    }
}