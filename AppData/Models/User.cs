﻿using AppData.Models.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppData.Models
{
    public class User
    {
        public int id { get; set; }
        [Required(ErrorMessage = "{0} Is Required")]
        [StringLength(15, ErrorMessage = "The {0} must be between {1} and {2} characters long.", MinimumLength = 5)]
        public String Username { get; set; }
        [Required(ErrorMessage = "{0} is required")]
        [MD5Valid(ErrorMessage = "The {0} is not MD5 String")]
        public String Password { get; set; }
        [Required]
        [StringLength(15, ErrorMessage = "The {0} must be between {1} and {2} characters long.", MinimumLength = 5)]
        public String Name { get; set; }
        [Required(ErrorMessage = "{0} Is Required")]
        [StringLength(15, ErrorMessage = "The {0} must be between {1} and {2} characters long.", MinimumLength = 5)]
        public String Surname { get; set; }
        [Required(ErrorMessage = "E-Mail Is Required")]
        [StringLength(40, ErrorMessage = "The {0} must be between {1} and {2} characters long.", MinimumLength = 5)]
        [EmailAddress]
        public String EMail { get; set; }
        public DateTime BirthDate { get; set; }
    }
}