﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppData.Models.Responses
{
    public class UserResponse : BaseResponse
    {
        public List<RegisterUserErrorResponse> ValidationErrors { get; set; }
    }
}