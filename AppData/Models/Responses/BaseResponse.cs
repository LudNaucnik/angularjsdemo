﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppData.Models.Responses
{
    public class BaseResponse
    {
        public Status Status { get; set; }
        public String Message { get; set; }
    }
    public enum Status
    {
        Valid,
        Invalid
    }
}