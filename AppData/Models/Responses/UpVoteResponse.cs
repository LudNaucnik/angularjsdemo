﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppData.Models.Responses
{
    public class UpVoteResponse : BaseResponse
    {
        public VoteStatus VoteStatus { get; set; }
    }

    public enum VoteStatus
    {
        Success,
        Failed,
        DoubleIncrement,
        SameFlag
    }
}