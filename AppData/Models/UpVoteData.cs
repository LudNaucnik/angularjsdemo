﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppData.Models
{
    public class UpVoteData
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public String Flag { get; set; }
        public int SessionID { get; set; }
    }
}