﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppData.Models
{
    public class Event
    {
        public Event()
        {
            sessions = new List<Session>();
            location = new Location();
        }
        public int id { get; set; }
        public String name { get; set; }
        public DateTime date { get; set; }
        public String time { get; set; }
        public virtual Location location { get; set; }
        public String imageUrl { get; set; }
        public virtual ICollection<Session> sessions { get; set; }
    }
}