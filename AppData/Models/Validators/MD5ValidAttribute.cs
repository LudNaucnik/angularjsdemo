﻿using AppData.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppData.Models.Validators
{
    public class MD5ValidAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            String strValue = value as string;
            if (!MD5Helper.IsValidMD5(strValue))
            {
                return false;
            }

            return true;
        }
    }
}