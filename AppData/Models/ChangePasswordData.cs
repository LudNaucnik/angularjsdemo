﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppData.Models
{
    public class ChangePasswordData
    {
        public String Username { get; set; }
        public String OldPassword { get; set; }
        public String NewPassword { get; set; }
    }
}