﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AppData.Models
{
    public class Location
    {
        public String address { get; set; }
        public String city { get; set; }
        public String province { get; set; }
    }
}