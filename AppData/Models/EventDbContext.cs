﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AppData.Models
{
    public class EventDbContext : DbContext
    {
        public EventDbContext() : base("eventsDbConn") { }

        public DbSet<Event> Events { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UpVoteData> UpVotes { get; set; }
    }
}