﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppData.Models
{
    public class Session
    {
        public int id { get; set; }
        public String name { get; set; }
        public String creatorName { get; set; }
        public int duration { get; set; }
        public String level { get; set; }
        public String abstractText { get; set; }
        public int upVoteCount { get; set; }
    }
}