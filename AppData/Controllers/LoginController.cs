﻿using AppData.DAL;
using AppData.Models;
using AppData.Models.Responses;
using AppData.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.ModelBinding;

namespace AppData.Controllers
{
    [EnableCors(origins: "http://localhost:9000", headers: "*", methods: "*")]
    public class LoginController : ApiController
    {
        [HttpPost]
        [Route("getUser")]
        public UserResponse getUser(User user)
        {
            UserResponse response = new UserResponse
            {
                Status = Models.Responses.Status.Valid,
                Message = String.Empty
            };
            try
            {
                UserActions.GetUser(user);
                response.Message = "user logged in";
            }
            catch (Exception)
            {
                response.Status = Models.Responses.Status.Invalid;
                response.Message = "Invalid username or password";
            }

            return response;
        }

        [HttpPost]
        [Route("register")]
        public UserResponse register(User user)
        {
            UserResponse response = new UserResponse
            {
                Status = Models.Responses.Status.Valid,
                Message = string.Empty,
                ValidationErrors = new List<RegisterUserErrorResponse>()

            };
            //UserStatus Status = new UserStatus();
            using (var db = new EventDbContext())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        UserActions.AddUser(user);
                        response.Message = "Model State Valid, User created: " + user.Username;
                        return response;
                    }

                    response.Status = Status.Invalid;
                    response.Message = "Registration Failed";

                    foreach (var modelStateKey in ModelState.Keys)
                    {
                        var modelStateVal = ModelState[modelStateKey];
                        foreach (var error in modelStateVal.Errors)
                        {
                            RegisterUserErrorResponse ValidationError = new RegisterUserErrorResponse
                            {
                                modelFieldKey = modelStateKey,
                                Message = error.ErrorMessage,
                                Status = Status.Invalid
                            };
                            response.ValidationErrors.Add(ValidationError);
                        }
                    }

                    // old code before validation
                    //if ((!String.IsNullOrEmpty(user.Username) || !String.IsNullOrEmpty(user.Password)) && MD5Helper.IsValidMD5(user.Password))
                    //{

                    //    if (UserActions.CheckUsername(user.Username) == false)
                    //    {
                    //        UserActions.AddUser(user);
                    //        response.Message = "user created: " + user.Username;
                    //    }
                    //    else
                    //    {
                    //        response.Status = Models.Responses.Status.Invalid;
                    //        response.Message = "duplicate username: " + user.Username;
                    //    }
                    //    //Status.Status = "user created" + user.MD5User;
                    //}
                    //else
                    //{
                    //    response.Status = Models.Responses.Status.Invalid;
                    //    response.Message = "not valid md5: " + user.Username;
                    //    //Status.Status = "not valid md5" + user.MD5User;
                    //}
                }
                catch (Exception ex)
                {
                    response.Status = Models.Responses.Status.Invalid;
                    response.Message = "User not created: " + user.Username + " Exception:" + ex.Message;
                    // old code
                    //Status.Status = "user not created" + user.MD5User;
                }
            }
            return response;
        }

        [HttpPost]
        [Route("changepassword")]
        public UserResponse changepassword(ChangePasswordData data)
        {
            UserResponse response = new UserResponse
            {
                Status = Models.Responses.Status.Valid,
                Message = string.Empty
            };

            if ((!String.IsNullOrEmpty(data.Username) || !String.IsNullOrEmpty(data.OldPassword) || !String.IsNullOrEmpty(data.NewPassword)) && MD5Helper.IsValidMD5(data.NewPassword))
            {
                if (UserActions.ChangePassword(data) == true)
                {
                    response.Message = "Password changed successfully";

                }
                else
                {
                    response.Status = Models.Responses.Status.Invalid;
                    response.Message = "Password not changed: Invalid username or password";
                }
            }
            else
            {
                response.Status = Models.Responses.Status.Invalid;
                response.Message = "Password not changed: password or username empty";
            }

            return response;
        }

        [HttpPost]
        [Route("edituserprofile")]
        public UserResponse EditProfile(User user)
        {
            UserResponse response = new UserResponse
            {
                Status = Models.Responses.Status.Valid,
                Message = string.Empty
            };
            if (getUser(user).Status == Status.Valid)
            {
                if (UserActions.EditUserProfile(user) == true)
                {
                    response.Message = "Profile Edit Success";
                }
                else
                {
                    response.Status = Models.Responses.Status.Invalid;
                    response.Message = "Profile Edit Failed";
                }

            }
            else
            {
                response.Status = Models.Responses.Status.Invalid;
                response.Message = "Invalid username or password";
            }
            return response;
        }

        [HttpPost]
        [Route("getuserdata")]
        public User getUsernameData(User user)
        {
            return UserActions.getUserData(user);
        }
    }
}
