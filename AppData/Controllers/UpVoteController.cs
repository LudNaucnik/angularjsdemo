﻿using AppData.DAL;
using AppData.Models;
using AppData.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AppData.Controllers
{
    [EnableCors(origins: "http://localhost:9000", headers: "*", methods: "*")]
    public class UpVoteController : ApiController
    {
        [HttpPost]
        [Route("VoteSession")]
        public UpVoteResponse UpVoteSession(UpVoteData data)
        {
            UpVoteResponse response = new UpVoteResponse
            {
                Status = Status.Valid,
                Message = String.Empty,
                VoteStatus = VoteStatus.Success
            };
            if (data.Flag == "upvote")
            {
                response = UpVoteActions.UpvoteSession(data);
                //if (UpVoteActions.UpvoteSession(data) == true)
                //{
                //    response.Message = "UpVote Success";
                //}
                //else
                //{
                //    response.Status = Status.Invalid;
                //    response.Message = "UpVote Failed";
                //}
            }
            else if (data.Flag == "downvote")
            {
                response = UpVoteActions.DownVoteSession(data);
                //if (UpVoteActions.DownVoteSession(data) == true)
                //{
                //    response.Message = "DownVote Success";
                //}
                //else
                //{
                //    response.Status = Status.Invalid;
                //    response.Message = "DownVote Failed";
                //}
            }
            else
            {
                response.Status = Status.Invalid;
                response.Message = "Invalid Flag";
                response.VoteStatus = VoteStatus.Failed;
            }
            return response;   
        }  
    }
}
