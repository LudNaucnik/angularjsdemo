﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;
using AppData.Models;
using AppData.Models.Responses;
using AppData.Utility;
using System.Threading;
using AppData.DAL;

namespace AppData.Controllers
{
    [EnableCors(origins: "http://localhost:9000", headers: "*", methods: "*")]
    public class EventController : ApiController
    {
        [HttpGet]
        public JToken getSingleEvent(int id)
        {
            try
            {
                String EventJson = "";
                using (var db = new EventDbContext())
                {
                    var evn = (from u in db.Events where u.id == id select u).First();
                    EventJson = JsonConvert.SerializeObject(evn);
                }
                return JObject.Parse(EventJson);
            }
            catch (Exception)
            {
                return JObject.Parse("{}");
            }

            //Reading from HDD
            //var path = HostingEnvironment.MapPath("/");
            //return JObject.Parse(File.ReadAllText(path + "../app/data/event/" + id + ".json"));
        }

        [HttpPost]
        [Route("saveEvent")]
        public HttpResponseMessage save(JObject eventData)
        {
            try
            {
                Event EventObj = JsonConvert.DeserializeObject<Event>(eventData.ToString());
                using (var db = new EventDbContext())
                {
                    db.Events.Add(EventObj);
                    db.SaveChanges();
                }
                return Request.CreateResponse(HttpStatusCode.OK, EventObj);
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            //Reading from HDD
            //var path = HostingEnvironment.MapPath("/");
            //File.WriteAllText(path + "../app/data/event/" + id + ".json", eventData.ToString(Formatting.None));
            //return Request.CreateResponse(HttpStatusCode.OK, "Event Id:" + id.ToString());
        }

        [HttpGet]
        [Route("getAllEvents")]
        public JArray getAllEvents()
        {
            try
            {
                String EventsJson = "";
                using (var db = new EventDbContext())
                {
                    EventsJson = JsonConvert.SerializeObject(db.Events.ToList());
                }
                return JArray.Parse(EventsJson);
            }
            catch (Exception)
            {
                return JArray.Parse("[{}]");
            }

            //Reading from HDD
            //var path = HostingEnvironment.MapPath("/");
            //var contents = "";

            //foreach (var file in Directory.GetFiles(path + ".../app/data/event/"))
            //{
            //    contents += File.ReadAllText(file) + ",";
            //}

            //return JArray.Parse("[" + contents.Substring(0, contents.Length - 1) + "]");
        }

        [HttpGet]
        [Route("getEventsIds")]
        public JArray GetEventIds()
        {
            try
            {
                String EventIDS = "";
                using (var db = new EventDbContext())
                {
                    var ids = (from u in db.Events select u.id).ToList();
                    EventIDS = JsonConvert.SerializeObject(ids);
                }
                return JArray.Parse(EventIDS);
            }
            catch (Exception)
            {
                return JArray.Parse("[{}]");
            }
        }   

        [HttpPost]
        [Route("editevent")]
        public BaseResponse EditEvent(Event evn)
        {
            BaseResponse res = new BaseResponse
            {
                Status = Status.Valid,
                Message = String.Empty
            };
            if(EventActions.EditEvent(evn) == true)
            {
                res.Message = "Event Edited Success";
            }
            else
            {
                res.Status = Status.Invalid;
                res.Message = "Event Edit Failed";
            }
            return res;
        }
    }
}
