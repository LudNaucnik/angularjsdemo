﻿using AppData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;

namespace AppData.DAL
{
    public class EventActions
    {
        public static EventDbContext db = new EventDbContext();
        public static Boolean EditEvent(Event evn)
        {
            try
            {
                Event EditEvent = db.Events.Where(u => u.id == evn.id).Include(p => p.sessions).SingleOrDefault();
                if (EditEvent != null)
                {
                    db.Entry(EditEvent).CurrentValues.SetValues(evn);
                }

                foreach (var existingSession in EditEvent.sessions.ToList())
                {
                    if (!evn.sessions.Any(c => c.id == existingSession.id))
                    {
                        EditEvent.sessions.Remove(existingSession);
                    }
                }

                // Update and Insert children
                foreach (var ses in evn.sessions)
                {
                    var existingChild = EditEvent.sessions
                        .Where(c => c.id == ses.id)
                        .SingleOrDefault();

                    if (existingChild != null)
                    {
                        // Update child
                        db.Entry(existingChild).CurrentValues.SetValues(ses);
                    }
                    else
                    {
                        // Insert child
                        var newChild = new Session
                        {
                            name = ses.name,
                            creatorName = ses.creatorName,
                            abstractText = ses.abstractText,
                            duration = ses.duration,
                            level = ses.level,
                            upVoteCount = ses.upVoteCount
                        };
                        EditEvent.sessions.Add(newChild);
                    }
                }

                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}