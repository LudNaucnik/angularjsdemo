﻿using AppData.Models;
using AppData.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppData.DAL
{
    public class UserActions
    {
        public static EventDbContext db = new EventDbContext();
        public static void AddUser(User newUser)
        {
            db.Users.Add(newUser);
            db.SaveChanges();
        }

        public static void GetUser(User UserToCheck)
        {
            var userTest = (from u in db.Users where u.Username == UserToCheck.Username && u.Password == UserToCheck.Password select u).First();
        }

        public static Boolean CheckUsername(String Username)
        {
            try
            {
                if ((from u in db.Users where u.Username == Username select u).Count() != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static Boolean ChangePassword(ChangePasswordData data)
        {
            try
            {
                if(!MD5Helper.IsValidMD5(data.OldPassword) || !MD5Helper.IsValidMD5(data.NewPassword))
                {
                    return false;
                }
                User usr = (from u in db.Users where u.Username == data.Username && u.Password == data.OldPassword select u).First();
                usr.Password = data.NewPassword;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static Boolean EditUserProfile(User user)
        {
            try
            {
                User usr = (from u in db.Users where u.Username == user.Username select u).First();
                usr.Name = user.Name;
                usr.Surname = user.Surname;
                usr.EMail = user.EMail;
                usr.BirthDate = user.BirthDate;
                db.SaveChanges();
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public static User getUserData(User user)
        {
            User usrData = new User();
            try
            {
                var usr = (from u in db.Users where u.Username == user.Username select u).First();
                usrData.Name = usr.Name;
                usrData.Surname = usr.Surname;
                usrData.BirthDate = usr.BirthDate;
                usrData.EMail = usr.EMail;
                return usrData;
            }
            catch(Exception)
            {
                return usrData;
            }
        }
    }
}