﻿using AppData.Models;
using AppData.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppData.DAL
{
    public class UpVoteActions
    {
        public static EventDbContext db = new EventDbContext();
        public static UpVoteResponse UpvoteSession(UpVoteData data)
        {
            Boolean sessionFound = false;
            UpVoteResponse response = new UpVoteResponse
            {
                Status = Status.Valid,
                Message = String.Empty,
                VoteStatus = VoteStatus.Success
            };
            try
            {
                int dbData = (from u in db.UpVotes where u.UserID == data.UserID && u.SessionID == data.SessionID && u.Flag == "upvote" select u).Count();
                if (dbData == 0)
                {
                    var sessions = (from u in db.Events select u.sessions).ToList();
                    for (int i = 0; i < sessions.Count(); i++)
                    {
                        foreach (Session ses in sessions.ElementAt(i))
                        {
                            if (ses.id == data.SessionID)
                            {
                                var downVoteCheck = (from u in db.UpVotes where u.UserID == data.UserID && u.SessionID == data.SessionID && u.Flag == "downvote" select u);
                                if (downVoteCheck.Count() != 0)
                                {
                                    ses.upVoteCount += 1;
                                    db.UpVotes.Remove(downVoteCheck.First());
                                    response.VoteStatus = VoteStatus.DoubleIncrement;
                                }
                                db.UpVotes.Add(data);
                                ses.upVoteCount += 1;
                                db.SaveChanges();
                                response.Message = "Vote Success";
                                sessionFound = true;
                                break;
                            }
                        }                        
                    }
                    if (!sessionFound)
                    {
                        response.Status = Status.Invalid;
                        response.Message = "Session not Found";
                        response.VoteStatus = VoteStatus.Failed;
                    }
                }
                else
                {
                    var sessions = (from u in db.Events select u.sessions).ToList();
                    for (int i = 0; i < sessions.Count(); i++)
                    {
                        foreach (Session ses in sessions.ElementAt(i))
                        {
                            if (ses.id == data.SessionID)
                            {
                                var downVoteCheck = (from u in db.UpVotes where u.UserID == data.UserID && u.SessionID == data.SessionID && u.Flag == "upvote" select u);
                                db.UpVotes.Remove(downVoteCheck.First());
                                ses.upVoteCount -= 1;
                                db.SaveChanges();
                                response.Message = "Vote Success";
                                response.VoteStatus = VoteStatus.SameFlag;
                                sessionFound = true;
                                break;
                            }
                        }
                    }
                    if (!sessionFound)
                    {
                        response.Status = Status.Invalid;
                        response.Message = "Session not Found";
                        response.VoteStatus = VoteStatus.Failed;
                    }
                }
            }
            catch (Exception)
            {
                response.Status = Status.Invalid;
                response.Message = "Error In Upvoting";
                response.VoteStatus = VoteStatus.Failed;
            }
            return response;
        }

        public static UpVoteResponse DownVoteSession(UpVoteData data)
        {
            Boolean sessionFound = false;
            UpVoteResponse response = new UpVoteResponse
            {
                Status = Status.Valid,
                Message = String.Empty,
                VoteStatus = VoteStatus.Success
            };
            try
            {
                int dbData = (from u in db.UpVotes where u.UserID == data.UserID && u.SessionID == data.SessionID && u.Flag == "downvote" select u).Count();
                if (dbData == 0)
                {

                    var sessions = (from u in db.Events select u.sessions).ToList();
                    for (int i = 0; i < sessions.Count(); i++)
                    {
                        foreach (Session ses in sessions.ElementAt(i))
                        {
                            if (ses.id == data.SessionID)
                            {
                                var downVoteCheck = (from u in db.UpVotes where u.UserID == data.UserID && u.SessionID == data.SessionID && u.Flag == "upvote" select u);
                                if (downVoteCheck.Count() != 0)
                                {
                                    ses.upVoteCount -= 1;
                                    db.UpVotes.Remove(downVoteCheck.First());
                                    response.VoteStatus = VoteStatus.DoubleIncrement;
                                }
                                db.UpVotes.Add(data);
                                ses.upVoteCount -= 1;
                                db.SaveChanges();
                                response.Message = "Vote Success";                                
                                sessionFound = true;
                                break;
                            }
                        }
                    }
                    if (!sessionFound)
                    {
                        response.Status = Status.Invalid;
                        response.Message = "Session not Found";
                        response.VoteStatus = VoteStatus.Failed;
                    }
                }
                else
                {
                    var sessions = (from u in db.Events select u.sessions).ToList();
                    for (int i = 0; i < sessions.Count(); i++)
                    {
                        foreach (Session ses in sessions.ElementAt(i))
                        {
                            if (ses.id == data.SessionID)
                            {
                                var upVoteCheck = (from u in db.UpVotes where u.UserID == data.UserID && u.SessionID == data.SessionID && u.Flag == "downvote" select u);
                                db.UpVotes.Remove(upVoteCheck.First());
                                ses.upVoteCount += 1;
                                db.SaveChanges();
                                response.Message = "Vote Success";
                                response.VoteStatus = VoteStatus.SameFlag;
                                sessionFound = true;
                                break;
                            }
                        }
                    }
                    if (!sessionFound)
                    {
                        response.Status = Status.Invalid;
                        response.Message = "Session not Found";
                        response.VoteStatus = VoteStatus.Failed;
                    }
                }
            }
            catch (Exception)
            {
                response.Status = Status.Invalid;
                response.Message = "Error In Dwwnvoting";
                response.VoteStatus = VoteStatus.Failed;
            }
            return response;
        }
    }
}